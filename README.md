## 接口列表

项目提供操作黑名单的web接口。
地址:http://43.254.44.139:8000

#### 浏览列表(web浏览器)

接口URL | 接口参数 | 描述
----|------|----
/view/black_list |无| 黑名单
/view/white_list | 无  | 白名单
/view/keyword_list | 无  | 关键字
/view/recommend_list | 无  | 推荐列表


#### 新增列表元素

接口URL | 接口参数 | 描述
----|------|----
/add/black_list/ |user_name| 黑名单
/add/white_list/ | user_name  | 白名单
/add/keyword_list/ | user_name  | 关键字
/add/recommend_list/ | user_name  | 推荐列表

#### 删除列表元素

接口URL | 接口参数 | 描述
----|------|----
/del/black_list/ |user_name| 黑名单
/del/white_list/ | user_name  | 白名单
/del/keyword_list/ | user_name  | 关键字
/del/recommend_list/ | user_name  | 推荐列表


#### 获取列表元素（json）

接口URL | 接口参数 | 描述
----|------|----
/get/black_list |无| 黑名单
/get/white_list | 无  | 白名单
/get/keyword_list | 无  | 关键字
/get/recommend_list | 无  | 推荐列表

## Example

#### 新增列表元素
```
http://43.254.44.139:8000/add/black_list/zsf
```


#### 删除列表元素
```
http://43.254.44.139:8000/del/black_list/zsf
```

#### 获取列表元素
```
http://43.254.44.139:8000/get/black_list
#Python调用详见testapi.py文件
```

