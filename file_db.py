import json

FILE_LIST = ['white_name_list.txt', 'black_name_list.txt', 'keyword_list.txt', 'recommend_list.txt']


class FileDB:
    @classmethod
    def load_list(cls):
        name_list = []
        for file_name in FILE_LIST:
            with open(file_name, 'r') as white_r:
                name_list.append(json.loads(white_r.readline()))
        return name_list[0], name_list[1], name_list[2], name_list[3]

    @classmethod
    def write_list(cls, white_name_list, black_name_list, keyword_list, recommend_list):
        """
        :param white_name_list: 
        :param black_name_list: 
        :param keyword_list: 
        :param recommend_list: 
        :return: 
        """
        with open(FILE_LIST[0], 'w') as f:
            f.write(json.dumps(white_name_list, ensure_ascii=False))
        with open(FILE_LIST[1], 'w') as f:
            f.write(json.dumps(black_name_list, ensure_ascii=False))
        with open(FILE_LIST[2], 'w') as f:
            f.write(json.dumps(keyword_list, ensure_ascii=False))
        with open(FILE_LIST[3], 'w') as f:
            f.write(json.dumps(recommend_list, ensure_ascii=False))
